# Satisfactory save share

## Prerequisites

### Create a GCP service account

- [ ] Open the [GCP console](console.cloud.google.com) and create a new project if needed.
- [ ] Then go to IAM > Service Accounts and create a new service account. Grant it the `Storage Object Admin` role.
- [ ] Open the service account details and go to the Keys tab. Create a new JSON key and save it for later.

### Create a shared folder on Google Drive

- [ ] Open [Google Drive](drive.google.com) and create a new folder.
- [ ] Share the folder with your service account using its email address (`xxx@yyy.iam.gserviceaccount.com`).

## Get started

Install packages with either `yarn` or `npm install`.

Launch the app with `yarn start` or `npm run start`.

## Configuration

### 1. Locate your save file

Satisfactory files are usually located in `%LOCALAPPDATA%/FactoryGame/Saved/SaveGames/`. Locate the specific save that you would like to share and copy its full path.

### 2. Copy the key.json file in the app root directory

Ask your buddy to send you the file if they already completed the prerequisites for you.

### 3. Find your shared Drive folder ID

Open your shared folder in Google Drive and copy its ID from the URL (`drive.google.com/drive/folders/<id is here>`).

### 4. Setup share configuration

Launch the app and select "Configure". Copy the save file path and the folder ID when prompted to do so.

## Usage

Launch the app and select "Sync save file". Your file will be either uploaded to the drive or downloaded from the drive depending on the most recent modification date.
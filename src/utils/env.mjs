export const getEnvVar = (
  envKey,
  {
    prefix = process.env.ENV_PREFIX,
  } = {},
) => {
  const envKeyPrefixed = `${prefix || ''}${envKey}`;

  const value = process.env[envKeyPrefixed];

  if (!value) {
    throw new Error(`Missing environment variable '${envKeyPrefixed}'`);
  }

  return value;
};

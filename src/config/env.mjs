import fs from 'fs';

import { join } from 'path';

let config = null;

const configFilePath = join(process.cwd(), './config.json');

export const getConfig = () => config;

export const loadConfig = () => {
  try {
    config = JSON.parse(fs.readFileSync(configFilePath, 'utf8'));
  } catch (err) {
    console.log('No valid configuration file found');
  }

  return config;
};

export const saveConfig = (newConfig) => {
  config = newConfig;
  fs.writeFileSync(configFilePath, JSON.stringify(config, null, 2));
};

import inquirer from 'inquirer';
import fs from 'fs';

import { loadConfig, saveConfig } from './config/env.mjs';
import { bootstrapDriveClient } from './services/google/drive.mjs';
import { syncSave } from './domain/sync/index.mjs';

(async function main() {
  const config = loadConfig();
  let target = 'configure';

  if (config) {
    ({
      target,
    } = await inquirer.prompt([
      {
        type: 'list',
        name: 'target',
        message: 'What do you want to do?',
        choices: [
          {
            name: 'Sync save file',
            value: 'sync',
          },
          {
            name: 'Configure',
            value: 'configure',
          },
        ],
      },
    ]));
  }

  if (target === 'configure') {
    const {
      savePath,
      driveFolderId,
    } = await inquirer.prompt([
      {
        type: 'input',
        name: 'savePath',
        message: 'Where is your save file located?',
      },
      {
        type: 'input',
        name: 'driveFolderId',
        message: 'What is the ID of the folder on drive?',
      },
    ]);

    saveConfig({
      saveFile: {
        path: savePath,
      },
      drive: {
        folderId: driveFolderId,
      },
    });
  } else {
    bootstrapDriveClient({ key: JSON.parse(fs.readFileSync('key.json')) });
    await syncSave();
  }
}());

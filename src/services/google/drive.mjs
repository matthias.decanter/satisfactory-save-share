import { google } from 'googleapis';

let client = null;

export const getDriveClient = () => client;

export const bootstrapDriveClient = (config) => {
  // Build the OAuth2 client.
  const oAuth2Client = new google.auth.JWT(
    config.key.client_email,
    null,
    config.key.private_key,
    ['https://www.googleapis.com/auth/drive'],
  );

  client = google.drive({ version: 'v3', auth: oAuth2Client });

  return client;
};

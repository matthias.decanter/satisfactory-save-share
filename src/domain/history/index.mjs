import fs from 'fs';
import path from 'path';

export const createBackupFile = (filePath) => {
  const fileName = path.basename(filePath);
  const ext = path.extname(fileName);
  const baseName = path.basename(filePath, ext);
  const directory = path.dirname(filePath);

  const backupFileName = `${baseName}-backup${ext}`;
  const backupFilePath = path.join(directory, backupFileName);

  try {
    fs.copyFileSync(
      filePath,
      backupFilePath,
    );

    console.log('Created local backup file:', backupFilePath);
  } catch (err) {
    console.log(err);
  }
};

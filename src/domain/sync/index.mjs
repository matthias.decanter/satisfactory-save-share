import fs from 'fs';
import path from 'path';
import dayjs from 'dayjs';

import inquirer from 'inquirer';
import { getConfig } from '../../config/env.mjs';
import { getDriveClient } from '../../services/google/drive.mjs';
import { createBackupFile } from '../history/index.mjs';

export const syncSave = async () => {
  const config = getConfig();
  const sharedFolderId = config.drive.folderId;
  const pathToSaveFile = config.saveFile.path;

  const fileName = path.basename(pathToSaveFile);

  const drive = getDriveClient();

  try {
    // Create the file metadata.
    const fileMetadata = {
      name: fileName,
      parents: [sharedFolderId],
    };

    // Create the media body.
    const media = {
      mimeType: 'text/plain',
      body: fs.createReadStream(pathToSaveFile),
    };

    const localExists = fs.existsSync(pathToSaveFile);

    const { data: { files } } = await drive.files.list({
      q: `parents in '${sharedFolderId}' and name='${fileName}'`,
      fields: 'nextPageToken, files(id, name, modifiedTime)',
    });

    if (files.length) {
      // File exists in drive.
      const [file] = files;
      console.log('Remove save file found:', file);

      let shouldDownload = true;

      if (localExists) {
        const { mtime: lastModified } = fs.statSync(pathToSaveFile);
        console.log('Local file was last modified:', lastModified);

        shouldDownload = dayjs(lastModified).isBefore(dayjs(file.modifiedTime));
      }

      if (shouldDownload) {
        const { confirmDownload } = await inquirer.prompt([
          {
            type: 'confirm',
            name: 'confirmDownload',
            message: 'The save file on drive is more recent than the local one, do you want to download it?',
          },
        ]);

        if (!confirmDownload) {
          console.log('Cancelling.');
          return;
        }

        createBackupFile(pathToSaveFile);

        // Download save from drive onto local filesystem.
        const dest = fs.createWriteStream(pathToSaveFile);
        const { data: fileData } = await drive.files.get(
          {
            fileId: file.id,
            alt: 'media',
          },
          {
            responseType: 'stream',
          },
        );
        fileData
          .on('end', () => {
            console.log(`The file was downloaded to ${pathToSaveFile}`);
          })
          .on('error', (err) => {
            console.log(`Error during download: ${err}`);
          })
          .pipe(dest);
      } else {
        const { confirmUpload } = await inquirer.prompt([
          {
            type: 'confirm',
            name: 'confirmUpload',
            message: 'Your local file is more recent than the one on drive, do you want to upload it?',
          },
        ]);

        if (!confirmUpload) {
          console.log('Cancelling.');
          return;
        }

        createBackupFile(pathToSaveFile);

        // Update the distant file with local save file.
        await drive.files.update({
          fileId: files[0].id,
          media,
        });

        console.log('Drive save has been updated with local file.');
      }
    } else if (localExists) {
      const { confirmUpload } = await inquirer.prompt([
        {
          type: 'confirm',
          name: 'confirmUpload',
          message: 'No save file was found on the drive, do you want to upload it?',
        },
      ]);

      if (!confirmUpload) {
        console.log('Cancelling.');
        return;
      }

      createBackupFile(pathToSaveFile);

      // Create file on drive.
      await drive.files.create({
        resource: fileMetadata,
        media,
        fields: 'id',
      });

      console.log('Local save has been copied on drive.');
    } else {
      console.error('Could not find save file in specified path!');
    }
  } catch (err) {
    console.log(err);
  }
};
